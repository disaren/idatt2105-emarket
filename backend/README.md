# Backend

This template should help get you started developing with Java Spring Boot and run tests in JUnit.


## Project Setup

Starts up the backend application
```sh
mvn spring-boot:run
```




### Run Unit Tests with [jUnit](https://junit.org/junit5/)

```sh
mvn test
```
