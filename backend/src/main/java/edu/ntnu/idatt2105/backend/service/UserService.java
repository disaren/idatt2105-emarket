package edu.ntnu.idatt2105.backend.service;


import edu.ntnu.idatt2105.backend.exceptions.UserAlreadyExistsException;
import edu.ntnu.idatt2105.backend.model.api.LoginBody;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import edu.ntnu.idatt2105.backend.model.api.LoginResponse;
import java.util.List;

/**
 * UserService class,
 * containing methods for user functionality
 */
public interface UserService {

  /**
   * Method getAllUsers
   * @return list with all users in database
   */
  public List<UserEntity> getAllUsers();

  /**
   * Method getUser
   * @param id long of wanted user
   * @return the user entity of the user with the corresponding id
   */
  public UserEntity getUser(Long id);

  /**
   * Method addUser
   * @param userEntity entity of the user which wants to add to database
   * @throws UserAlreadyExistsException if the user entity is already in the database
   */
  public void addUser(UserEntity userEntity) throws UserAlreadyExistsException;

  /**
   * Method loginResponse
   * @param loginBody containing information needed from user to log in
   * @return login response with positive or negative http status, depending on user logged in or not
   */
  public LoginResponse loginUser(LoginBody loginBody);

  /**
   * Method updateUserNameEmail
   * @param user entity of user which wants to update username and email
   * @param username string, of the new username
   * @param email string, of the new email
   */
  void updateUserNameEmail(UserEntity user, String username, String email);

  /**
   * Method updateUserPassword
   * @param user entity of the user which wants to update password
   * @param password string, of the new password
   */
  void updateUserPassword(UserEntity user, String password);
}
