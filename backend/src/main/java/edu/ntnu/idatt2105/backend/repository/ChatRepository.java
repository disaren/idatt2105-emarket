package edu.ntnu.idatt2105.backend.repository;

import edu.ntnu.idatt2105.backend.model.ChatEntity;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * ChatRepository class connecting Chat entity to database
 */
public interface ChatRepository extends JpaRepository<ChatEntity, Long> {
  /**
   * Method findByUserOne
   * @param user entity which represents the first user in the chat
   * @return a chat list of chat entities which contains user one
   */
  List<ChatEntity> findByUserOne(UserEntity user);

  /**
   * Method findByUserTwo
   * @param user entity which represents the second user in the chat
   * @return a chat list of chat entities which contains user two
   */
  List<ChatEntity> findByUserTwo(UserEntity user);

}
