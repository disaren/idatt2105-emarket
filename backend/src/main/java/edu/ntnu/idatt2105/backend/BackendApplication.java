package edu.ntnu.idatt2105.backend;


import edu.ntnu.idatt2105.backend.model.UserEntity;
import edu.ntnu.idatt2105.backend.model.enums.Category;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.sql.*;
import java.util.Base64;

/**
 * BackendApplication,
 * runs the spring application and adds default info to database
 */
@SpringBootApplication
public class BackendApplication {


    public static void main(String[] args) throws IOException {
        SpringApplication.run(BackendApplication.class, args);



        UserEntity adminUser = new UserEntity("Kari","kari123", "kari@gmail.com");
        adminUser.setModerator(true);


        URL url1 = new URL("http://localhost:8081/api/users/add");
        HttpURLConnection con1 = (HttpURLConnection) url1.openConnection();
        con1.setRequestMethod("POST");
        con1.setDoOutput(true);
        con1.setRequestProperty("Content-Type", "application/json");
        con1.setRequestProperty("User-Agent", "Mozilla/5.0");



        try (DataOutputStream dos = new DataOutputStream(con1.getOutputStream())) {
           dos.writeBytes(adminUser.toString());
            System.out.println("User added successfully to database....");

        }

        System.out.println("Response code: " + con1.getResponseCode());


        try {
            System.setProperty("jdbc.drivers","com.mysql.jdbc.Driver");
            Class.forName("com.mysql.jdbc.Driver").getDeclaredConstructor().newInstance();

            String myUrl = "jdbc:mysql://localhost/marketdb";
            Connection conn = DriverManager.getConnection(myUrl, "root", "");

            String sql = " insert into item (category, image, latitude, long_desc, longtitude, price, short_desc, user_id)"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?)";

            PreparedStatement preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setString(1, Category.FURNITURE.toString());

            File file = new File(System.getProperty("user.dir") + System.getProperty("file.separator") +
                    "src" + System.getProperty("file.separator") + "main" + System.getProperty("file.separator") +
                    "resources" + System.getProperty("file.separator") + "images"  + System.getProperty("file.separator") + "sofa.jpg");
            InputStream input = new ByteArrayInputStream(Base64.getEncoder().encodeToString(Files.readAllBytes(file.toPath())).getBytes());

            preparedStmt.setBlob(2, input);
            preparedStmt.setDouble(3, 63.4);
            preparedStmt.setString(4, "Grey sofa with black metal legs from IKEA. Nicely used, no damages.");
            preparedStmt.setDouble(5,10.4);
            preparedStmt.setDouble(6, 1099);
            preparedStmt.setString(7,"Grey sofa");
            preparedStmt.setInt(8,1);


            preparedStmt.execute();

            conn.close();

            System.out.println("Item added successfully to database....");

        }catch (Exception e)
        {
            System.err.println("Got an exception!");
            e.printStackTrace();
            System.out.println(e);
        }

    }
}

