package edu.ntnu.idatt2105.backend.service;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

/**
 * EncryptionService class,
 * containing methods for password functionality
 */
@Service
public class EncryptionService {

  @Value("${encryption.salt.rounds}")
  private int saltRounds;
  private String salt;

  @PostConstruct
  public void postConstruct(){
    salt = BCrypt.gensalt(saltRounds);
  }

  /**
   * Method encryptPassword
   * @param password string password of user
   * @return encrypted password string
   */
  public String encryptPassword(String password){
    return BCrypt.hashpw(password, salt);
  }

  /**
   * Method verifyPassword
   * @param password string password of user
   * @param hash password in database
   * @return boolean true or false, depending on if the user password is equal to the password in the database
   */
  public boolean verifyPassword(String password, String hash){
    return BCrypt.checkpw(password, hash);
  }

}
