package edu.ntnu.idatt2105.backend.model;


import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import edu.ntnu.idatt2105.backend.model.enums.Category;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;

/**
 * Item entity,
 * represents database table containing item object
 */
@Entity
@Table(name = "item")
public class ItemEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Column(name = "short_desc")
    private String shortDesc;
    @Size(max = 10000)
    @Column(name = "long_desc")
    private String longDesc;
    @Column
    @Enumerated(EnumType.STRING)
    private Category category;
    @Column
    private double latitude;
    @Column
    private double longtitude;
    @Column
    private double price;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private UserEntity user;
    @Lob
    @Column(columnDefinition = "MEDIUMBLOB")
    private String image;


    public ItemEntity(){

    }

    /**
     * ItemEntity constructor
     * @param name string name of item
     * @param shortDesc string short description of the item
     * @param longDesc string long description of the item
     * @param category string category of the item
     * @param latitude double longtitude of the location of the item
     * @param longtitude double longtitude of the location of the item
     * @param price double price of the item
     * @param user entity of the user that wants to sell the item
     * @param image base64 string of image of item
     */
    public ItemEntity(String name, String shortDesc, String longDesc, Category category, double latitude, double longtitude, double price,
                      UserEntity user, String image) {
        super();
        this.name = name;
        this.shortDesc = shortDesc;
        this.longDesc = longDesc;
        this.category = category;
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.price = price;
        this.user = user;
        this.image = image;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getLongDesc() {
        return longDesc;
    }

    public void setLongDesc(String longDesc) {
        this.longDesc = longDesc;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "ItemEntity{" +
                "id=" + id +
                ", shortDesc='" + shortDesc + '\'' +
                ", longDesc='" + longDesc + '\'' +
                ", category='" + category + '\'' +
                ", latitude=" + latitude +
                ", longtitude=" + longtitude +
                ", price=" + price +
                '}';
    }
}
