package edu.ntnu.idatt2105.backend.model.api;

/**
 * UserResponse class,
 *  returns information about the user without the password param
 */
public class UserResponse {
  private String username;
  private String email;


  private boolean moderator;

  /**
   * UserResponse constructor
   * @param username string username of user
   * @param email string email of user
   * @param moderator boolean depending on if user is moderator or not
   */
  public UserResponse(String username, String email, boolean moderator) {
    this.username = username;
    this.email = email;
    this.moderator = moderator;
  }

  public String getUsername() {
    return username;
  }

  public String getEmail() {
    return email;
  }


  public boolean isModerator() {
    return moderator;
  }
}
