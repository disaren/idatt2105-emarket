package edu.ntnu.idatt2105.backend.controller;

import edu.ntnu.idatt2105.backend.model.ItemEntity;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import edu.ntnu.idatt2105.backend.model.enums.Category;
import edu.ntnu.idatt2105.backend.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

/**
 * ItemController class,
 * containing endpoints handling items
 */
@RestController
@EnableAutoConfiguration
@CrossOrigin("*")
@RequestMapping(value = "/api/items")
public class ItemController {

  @Autowired
  ItemService itemService;

  /**
   * Methos upload
   * @param name param containing string name of item
   * @param shortDesc param containing string short description of item
   * @param longDesc param containing string long description of item
   * @param category param containing string category of item
   * @param latitude param containing double latitude of where item is located
   * @param longtitude param containing double longtitude of where item is located
   * @param price param containing double price of item
   * @param image param containing multipart file of item
   * @param user authentication tolkin for specific user entity
   * @return the http response
   * @throws IOException gets thrown if there are faults in inputs
   */
  @PostMapping("/upload")
  public ResponseEntity<?> upload(@RequestParam("name")String name, @RequestParam("shortDesc") String shortDesc, @RequestParam("longDesc") String longDesc, @RequestParam("category") String category,
                               @RequestParam("latitude") double latitude, @RequestParam("longtitude") double longtitude, @RequestParam("price") double price,
                               @RequestParam("image")MultipartFile image, @AuthenticationPrincipal UserEntity user) throws IOException {
    itemService.saveOrUpdate(new ItemEntity(name, shortDesc,longDesc, Category.valueOf(category),latitude,
            longtitude,price,user, Base64.getEncoder().encodeToString(image.getBytes())));
    return ResponseEntity.ok().build();
  }

  /**
   * Method list
   * @return a list of all items in database
   */
  @GetMapping("/list")
  public List<ItemEntity> list(){
    return itemService.getAllItems();
  }

  /**
   * Method getItemById
   * @param id long id of a specific item
   * @return the wanted item or bad http request, depending on it the item is in the database or not
   */
  @GetMapping("/list/{id}")
  public ResponseEntity<ItemEntity> getItemById(@PathVariable Long id){
    ItemEntity item = itemService.getItem(id);
    if(item == null){
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }else{
      return ResponseEntity.ok(item);
    }
  }

  /**
   * Method getItemsByUser
   * @param user authentication tolkin for specific user entity
   * @return a list with all the items a user is selling
   */
  @GetMapping("/list/me")
  public List<ItemEntity> getItemsByUser(@AuthenticationPrincipal UserEntity user){
    return itemService.getItemByUser(user);
  }

  /**
   * Method getItemsByCategory
   * @param category param containing string category of item
   * @return all the items of the selected category
   */
  @GetMapping("/list/category")
  public List<ItemEntity> getItemsByCategory(@RequestParam("category") String category){
    return itemService.getItemByCategory(category);
  }

  /**
   * Method getCategories
   * @return all categories saved in the database
   */
  @GetMapping("categories")
  public List<Category> getCategories(){
    return Arrays.stream(Category.values()).toList();
  }

}
