package edu.ntnu.idatt2105.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;

/**
 * User entity,
 * represents database table containing user object
 */
@Entity
@Table(name = "user")
public class UserEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column
  @NotNull
  @NotBlank
  @Size(min = 3, max = 255)
  private String username;
  @Column
  @NotNull
  @NotBlank
  @Size(min = 6, max = 1000)
  private String password;
  @Column
  @Email
  @NotNull
  @NotBlank
  private String email;

  @Column
  private boolean moderator;



  public UserEntity(){

  }

  /**
   * UserEntity constructor
   * @param username string username of user
   * @param password string password of user
   * @param email string email of user
   */
  public UserEntity(String username, String password, String email) {
    super();
    this.username = username;
    this.password = password;
    this.email = email;
  }

  public Long getId() {
    return id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  @JsonIgnore
  public String getPassword() {
    return password;
  }
  @JsonProperty

  public void setPassword(String password) {
    this.password = password;
  }

  @JsonIgnore
  public String getEmail() {
    return email;
  }

  @JsonProperty
  public void setEmail(String email) {
    this.email = email;
  }

  @JsonIgnore
  public boolean isModerator() {
    return moderator;
  }

  @JsonProperty
  public void setModerator(boolean moderator) {
    this.moderator = moderator;
  }

  @Override
  public String toString() {
    return "{" + '\n' +
            "\"username\" : " +  "\""  + username +   "\","  + '\n' +
            "\"password\" : " +  "\""  + password +   "\","  + '\n' +
            "\"email\" : " +  "\""  + email +   "\""  + '\n' +
            "}";
  }
}
