package edu.ntnu.idatt2105.backend.exceptions;

/**
 * Exception which gets thrown if user is already in database
 */
public class UserAlreadyExistsException extends Exception{

}
