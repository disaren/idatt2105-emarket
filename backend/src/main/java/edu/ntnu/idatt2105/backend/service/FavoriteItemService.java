package edu.ntnu.idatt2105.backend.service;

import edu.ntnu.idatt2105.backend.model.FavoriteItemEntity;
import edu.ntnu.idatt2105.backend.model.ItemEntity;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import edu.ntnu.idatt2105.backend.repository.FavoriteItemsRepository;
import edu.ntnu.idatt2105.backend.repository.ItemRepository;
import edu.ntnu.idatt2105.backend.repository.UserRepository;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * FavoriteItemService class,
 * containing methods for item functionality
 */
@Service
public class FavoriteItemService {

    private ItemRepository itemRepository;
    private UserRepository userRepository;

    private FavoriteItemsRepository favoriteItemsRepository;

    /**
     * FavoriteItemService constructor
     * @param favoriteItemsRepository representing favorite item table in database
     * @param itemRepository representing item table in database
     * @param userRepository representing user table in database
     */
    public FavoriteItemService(FavoriteItemsRepository favoriteItemsRepository, ItemRepository itemRepository, UserRepository userRepository){
       this.favoriteItemsRepository = favoriteItemsRepository;
       this.itemRepository = itemRepository;
       this.userRepository = userRepository;
    }

    /**
     * Method addToFavorites
     * @param favoriteItemEntity entity of the favorite item which we want to save to the database, if the item is not already in database
     */
    public void addToFavorite(FavoriteItemEntity favoriteItemEntity) {
        if (!(favoriteItemsRepository.existsById(favoriteItemEntity.getItem().getId()) &&
                favoriteItemsRepository.existsById(favoriteItemEntity.getUser().getId()))) {

            System.out.println(favoriteItemEntity.getUser().getId());
            favoriteItemsRepository.save(favoriteItemEntity);
        }

    }

    /**
     * Method removeFromFavorite
     * @param userID the id of the user which we want to remove item from favorites
     * @param itemID the id of the item which we want to remove from favorites
     */
    public void removeFromFavorite(long userID, long itemID) {
        List <FavoriteItemEntity> favoritesByUser = favoriteItemsRepository.findFavoritesByUser(userRepository.getReferenceById(userID));
        for (FavoriteItemEntity favoriteItemEntity : favoritesByUser) {
            if (favoriteItemEntity.getItem().getId() == itemID) {
                favoriteItemsRepository.delete(favoriteItemEntity);
            }
        }
    }

    /**
     * Method getFavoritesByUser
     * @param user entity of the user we want to get all favorites
     * @return list of the favorite items of specific user
     */
    public List<ItemEntity> getFavoritesByUser(UserEntity user) {
        List<ItemEntity> favoriteItemByUser = new ArrayList<>();
        List <FavoriteItemEntity> favorites = favoriteItemsRepository.findFavoritesByUser(user);

        for (FavoriteItemEntity favorite : favorites) {
            favoriteItemByUser.add(itemRepository.getReferenceById(favorite.getItem().getId()));
        }
        return favoriteItemByUser;
    }

}
