package edu.ntnu.idatt2105.backend.model.api;

/**
 * Class MessageBody,
 * containing the information needed from user to send message
 */
public class MessageBody {

  private String username;
  private String message;

  public MessageBody(){

  }

  /**
   * MessageBody contructor
   * @param username string username of user sending message
   * @param message string message
   */
  public MessageBody(String username, String message) {
    super();
    this.username = username;
    this.message = message;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
