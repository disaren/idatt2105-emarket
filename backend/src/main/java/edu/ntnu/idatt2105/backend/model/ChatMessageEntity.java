package edu.ntnu.idatt2105.backend.model;

import jakarta.persistence.*;

/**
 * ChatMessage entity,
 * represents database table containing chat message object
 */
@Entity
@Table(name = "message")
public class ChatMessageEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "chat_id")
  private ChatEntity chat;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user_id")
  private UserEntity user;

  private String message;

  public ChatMessageEntity(){

  }

  /**
   * CharMessageEntity constructor
   * @param chat entity that the user wants to send message to
   * @param user entity that wants to send the message
   * @param message string message that the user wants to send
   */
  public ChatMessageEntity(ChatEntity chat, UserEntity user, String message) {
    super();
    this.chat = chat;
    this.user = user;
    this.message = message;
  }

  public Long getId() {
    return id;
  }

  public ChatEntity getChat() {
    return chat;
  }

  public void setChat(ChatEntity chat) {
    this.chat = chat;
  }

  public UserEntity getUser() {
    return user;
  }

  public void setUser(UserEntity user) {
    this.user = user;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
