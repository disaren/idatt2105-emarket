package edu.ntnu.idatt2105.backend.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.intercept.AuthorizationFilter;

/**
 * WebSecurityConfig class,
 * configurations spring security for http requests authentication
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

  private JWTRequestFilter jwtRequestFilter;

  /**
   * Constructor WebSecurituConfig
   * @param jwtRequestFilter sets new jwt request filter from param jwtRequestFilter
   */
  public WebSecurityConfig(JWTRequestFilter jwtRequestFilter) {
    this.jwtRequestFilter = jwtRequestFilter;
  }

  /**
   * Method filterChain
   * @param http security object
   * @return http request
   * @throws Exception if cant get or permit http
   */
  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    http.cors().and().csrf().disable();
    http.addFilterBefore(jwtRequestFilter, AuthorizationFilter.class);
    http.authorizeHttpRequests()
            .requestMatchers("/api/items/list").permitAll()
            .requestMatchers("/api/items/list/**").permitAll()
            .requestMatchers("/api/users/add").permitAll()
            .requestMatchers("/api/items/upload").permitAll()
            .requestMatchers("/api/users/auth/login").permitAll()
            .requestMatchers("/api/items/categories").permitAll()
            .anyRequest().authenticated();


    return http.build();
  }

}
