package edu.ntnu.idatt2105.backend.model;

import jakarta.persistence.*;
/**
 * FavoriteItem entity,
 * represents database table containing the favorite item of a user
 */
@Entity
@Table(name = "favorites")
public class FavoriteItemEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "item_id")
    private ItemEntity item;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private UserEntity user;


    public FavoriteItemEntity(){

    }

    /**
     * FavoriteItemEntity constructor
     * @param user entity which have the favorite item
     * @param item entity which is the favorite item of a user
     */
    public FavoriteItemEntity( UserEntity user, ItemEntity item) {
        this.user = user;
        this.item = item;
    }

    public Long getId() {
        return id;
    }

    public ItemEntity getItem() {
        return item;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setItem(ItemEntity item) {
        this.item = item;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
