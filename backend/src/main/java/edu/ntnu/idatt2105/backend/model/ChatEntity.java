package edu.ntnu.idatt2105.backend.model;

import jakarta.persistence.*;

/**
 * Chat entity,
 * represents database table containing chat object
 */
@Entity
@Table(name = "chat")
public class ChatEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user1_id")
  private UserEntity userOne;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user2_id")
  private UserEntity userTwo;

  public ChatEntity(){

  }

  /**
   * ChatEntity constructor
   * @param userOne entity one which shall be in the chat
   * @param userTwo entity two which shall be in the chat
   */
  public ChatEntity( UserEntity userOne, UserEntity userTwo) {
    super();
    this.userOne = userOne;
    this.userTwo = userTwo;
  }

  public Long getId() {
    return id;
  }

  public UserEntity getUserOne() {
    return userOne;
  }

  public void setUserOne(UserEntity userOne) {
    this.userOne = userOne;
  }

  public UserEntity getUserTwo() {
    return userTwo;
  }

  public void setUserTwo(UserEntity userTwo) {
    this.userTwo = userTwo;
  }
}
