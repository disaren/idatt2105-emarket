package edu.ntnu.idatt2105.backend.service;

import edu.ntnu.idatt2105.backend.exceptions.UserAlreadyExistsException;
import edu.ntnu.idatt2105.backend.model.api.LoginBody;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import edu.ntnu.idatt2105.backend.model.api.LoginResponse;
import edu.ntnu.idatt2105.backend.repository.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * UserService class,
 * containing methods for user functionality
 */
@Service
@Transactional
public class UserServiceImplement implements UserService{

  UserRepository userRepository;

  private EncryptionService encryptionService;
  private JWTService jwtService;


  /**
   * UserServiceImplements constructor
   * @param userRepository representing user table in database
   * @param encryptionService encryption service class
   * @param jwtService jwt service class
   */
  public UserServiceImplement(UserRepository userRepository, EncryptionService encryptionService, JWTService jwtService){
    this.userRepository = userRepository;
    this.encryptionService = encryptionService;
    this.jwtService = jwtService;
  }

  /**
   * Method getAllUsers
   * @return list with all users in database
   */
  @Override
  public List<UserEntity> getAllUsers() {
    return userRepository.findAll();
  }

  /**
   * Method getUser
   * @param id long of wanted user
   * @return the user entity of the user with the corresponding id
   */
  @Override
  public UserEntity getUser(Long id) {
    return userRepository.findById(id).orElse(null);
  }

  /**
   * Method updateUserNameEmail
   * @param user entity of user which wants to update username and email
   * @param username string, of the new username
   * @param email string, of the new email
   */
  @Override
  public void updateUserNameEmail(UserEntity user, String username, String email) {
    userRepository.updateUserNameEmail(username,email,user.getId());
  }

  /**
   * Method updateUserPassword
   * @param user entity of the user which wants to update password
   * @param password string, of the new password
   */
  @Override
  public void updateUserPassword(UserEntity user, String password) {
    userRepository.updateUserPassword(encryptionService.encryptPassword(password), user.getId());
  }

  /**
   * Method addUser
   * @param userEntity entity of the user which wants to add to database
   * @throws UserAlreadyExistsException if the user entity is already in the database
   */
  @Override
  public void addUser(UserEntity userEntity) throws UserAlreadyExistsException {
    if(userRepository.findByEmailIgnoreCase(userEntity.getEmail()).isPresent()
            || userRepository.findByUsernameIgnoreCase(userEntity.getUsername()).isPresent()){
      throw new UserAlreadyExistsException();
    }

    userEntity.setPassword(encryptionService.encryptPassword(userEntity.getPassword()));
    userRepository.save(userEntity);
  }


  /**
   * Method loginResponse
   * @param loginBody containing information needed from user to log in
   * @return login response with positive or negative http status, depending on user logged in or not
   */
  @Override
  public LoginResponse loginUser(LoginBody loginBody) {
    Optional<UserEntity> user = userRepository.findByUsernameIgnoreCase(loginBody.getUsername());
    if(user.isPresent()){
      UserEntity userEntity = user.get();
      if(encryptionService.verifyPassword(loginBody.getPassword(),userEntity.getPassword())){
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setJwt(jwtService.generateJWT(userEntity));
        loginResponse.setUsername(userEntity.getUsername());
        loginResponse.setEmail(userEntity.getEmail());
        loginResponse.setModerator(userEntity.isModerator());
        return loginResponse;
      }
    }
    return null;
  }

}
