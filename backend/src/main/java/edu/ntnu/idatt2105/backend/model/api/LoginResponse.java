package edu.ntnu.idatt2105.backend.model.api;

/**
 * LoginResponse class,
 * containing the information returned upon user logging in
 */
public class LoginResponse {
    private String username;
    private String email;
    private String jwt;
    private boolean moderator;

    public LoginResponse() {
    }

    /**
     * LoginResponse constructor
     * @param id long id unique for user
     * @param username string username unique for user
     * @param email string email of user
     * @param jwt security tolkin representing user
     * @param moderator boolen depending on if the user is moderator or not
     */
    public LoginResponse(String username, String email, String jwt, boolean moderator) {
        this.username = username;
        this.email = email;
        this.jwt = jwt;
        this.moderator = moderator;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public boolean isModerator() {
        return moderator;
    }

    public void setModerator(boolean moderator) {
        this.moderator = moderator;
    }
}
