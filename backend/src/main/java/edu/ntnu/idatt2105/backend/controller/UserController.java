package edu.ntnu.idatt2105.backend.controller;


import edu.ntnu.idatt2105.backend.exceptions.UserAlreadyExistsException;
import edu.ntnu.idatt2105.backend.model.api.LoginBody;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import edu.ntnu.idatt2105.backend.model.api.LoginResponse;
import edu.ntnu.idatt2105.backend.model.api.UserResponse;
import edu.ntnu.idatt2105.backend.service.UserService;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * UserController class,
 * containing endpoints handling users
 */
@RestController
@EnableAutoConfiguration
@CrossOrigin("*")
@RequestMapping("/api/users")
public class UserController {


    @Autowired
    UserService userService;
    private boolean hasModerator=false;

    /**
     * Method save
     * @param user requestbody containing user entity element
     * @return message "user added" or conflict http status, depending on if user gets added or if it already is registered
     */
    @PostMapping("/add")
    public ResponseEntity<?> save(@Valid @RequestBody UserEntity user){
        if(!hasModerator){
            user.setModerator(true);
            hasModerator = true;
        }else{
            user.setModerator(false);
        }

        try{
            userService.addUser(user);
            return ResponseEntity.ok().body("User added");
        }catch (UserAlreadyExistsException userAlreadyExistsException){
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

    /**
     * Method loginUser
     * @param loginBody request body containing necessary information for login
     * @return information about the user login in, or bad http status depending on if user is in database or not
     */
    @PostMapping("/auth/login")
    public ResponseEntity<LoginResponse> loginUser(@Valid @RequestBody LoginBody loginBody){
        LoginResponse loginResponse = userService.loginUser(loginBody);
        if(loginResponse == null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }else {
            return ResponseEntity.ok(loginResponse);
        }
    }

    /**
     * Method updateUserNameEmai
     * @param user authentication tolkin for specific user entity
     * @param username param string username of user
     * @param email param string email of user
     * @return response string "user updated successfully"
     */
    @PostMapping("/auth/update/userNameEmail")
    public ResponseEntity<?> updateUserNameEmail(@AuthenticationPrincipal UserEntity user, @RequestParam("username") String username,
        @RequestParam("email") String email){
        userService.updateUserNameEmail(user,username,email);
        return ResponseEntity.ok("User updated successfully");
    }

    /**
     * Method updateUserPassword
     * @param user authentication tolkin for specific user entity
     * @param password param containing string password of user
     * @return response entity "User updated successfully"
     */
    @PostMapping("/auth/update/password")
    public ResponseEntity<?> updateUserPassword(@AuthenticationPrincipal UserEntity user, @RequestParam("password") String password){
        userService.updateUserPassword(user,password);
        return ResponseEntity.ok("User updated successfully");
    }

    /**
     * Method getLoggedInUser
     * @param user authentication tolkin for specific user entity
     * @return username, email and moderator boolean of logged in user
     */
    @GetMapping("/auth/me")
    public UserResponse getLoggedInUser(@AuthenticationPrincipal UserEntity user){
        return new UserResponse(user.getUsername(), user.getEmail(), user.isModerator());
    }

    /**
     * Method list
     * @param user authentication tolkin for specific user entity
     * @return list of all users registered in the database if the user is moderator, or else http status unauthorized
     */
    @GetMapping("/list")
    public ResponseEntity<?> list(@AuthenticationPrincipal UserEntity user){
        if(user.isModerator()){
            return ResponseEntity.ok(userService.getAllUsers());
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized");
    }




}
