package edu.ntnu.idatt2105.backend.model.api;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

/**
 * LoginBody class,
 * containing the information needed from user to log in
 */
public class LoginBody {

  @NotBlank
  @NotNull
  private String username;
  @NotBlank
  @NotNull
  private String password;


  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

}
