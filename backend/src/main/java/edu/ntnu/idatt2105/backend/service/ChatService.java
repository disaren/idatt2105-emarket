package edu.ntnu.idatt2105.backend.service;

import edu.ntnu.idatt2105.backend.model.ChatEntity;
import edu.ntnu.idatt2105.backend.model.ChatMessageEntity;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import edu.ntnu.idatt2105.backend.repository.ChatRepository;
import edu.ntnu.idatt2105.backend.repository.MessageRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * ChatService class,
 * containing methods for chat functionality
 */
@Service
public class ChatService {

  private ChatRepository chatRepository;
  private MessageRepository messageRepository;

  /**
   * ChatService constructor
   * @param chatRepository representing chat table in database
   * @param messageRepository representing message table in database
   */
  public ChatService(ChatRepository chatRepository, MessageRepository messageRepository) {
    this.chatRepository = chatRepository;
    this.messageRepository = messageRepository;
  }

  /**
   * Method getAllChatsByUser
   * @param user entity of the user in which we want to get all chats
   * @return list of chat entities or null, depending on if the user has any chats
   */
  public List<ChatEntity> getAllChatsByUser(UserEntity user){
    List<ChatEntity> chatListOne = chatRepository.findByUserOne(user);
    List<ChatEntity> chatListTwo = chatRepository.findByUserTwo(user);

    chatListOne.addAll(chatListTwo);

    if(chatListOne.isEmpty()){
      return null;
    }

    return chatListOne;
  }

  /**
   * Method getAllMessagesFromChat
   * @param chatEntity entity of the chat in which we want go get all messages
   * @return list of messages from specific chat
   */
  public List<ChatMessageEntity> getAllMessagesFromChat(ChatEntity chatEntity){
    return messageRepository.findByChat(chatEntity);
  }

  /**
   * Method getChatById
   * @param id long of the specific chat
   * @return the chat entity of the chat with the specific id or null, depending on if the chat exist
   */
  public ChatEntity getChatById(Long id){
    return chatRepository.findById(id).orElse(null);
  }

  /**
   * Method saveChat
   * @param chatEntity entity of the chat we want to save to the database
   */
  public void saveChat(ChatEntity chatEntity){
    chatRepository.save(chatEntity);
  }

  /**
   * Method saveMessage
   * @param chatMessageEntity entity of the message we want to save to the database
   */
  public void saveMessage(ChatMessageEntity chatMessageEntity){
    messageRepository.save(chatMessageEntity);
  }

  /**
   * Method charAlreadyExist
   * @param userOne entity of user one in the chat
   * @param userTwo entity of the user two in the chat
   * @return boolean true or false, depending on if the chat with the two users is in the database
   */
  public boolean chatAlreadyExists(UserEntity userOne, UserEntity userTwo){
    List<ChatEntity> userOneChats = chatRepository.findByUserOne(userOne);
    List<ChatEntity> userTwoChats = chatRepository.findByUserTwo(userTwo);

    AtomicBoolean chatExists = new AtomicBoolean(false);
    userOneChats.forEach(chat -> {
      if(userTwoChats.contains(chat)){
        chatExists.set(true);
      }
    });

    userTwoChats.forEach(chat -> {
      if(userOneChats.contains(chat)){
        chatExists.set(true);
      }
    });

    return chatExists.get();
  }

}
