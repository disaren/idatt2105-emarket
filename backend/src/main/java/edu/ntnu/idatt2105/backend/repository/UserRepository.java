package edu.ntnu.idatt2105.backend.repository;

import edu.ntnu.idatt2105.backend.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

/**
 * UserRepository class connecting users entity to database
 */
public interface UserRepository extends JpaRepository<UserEntity, Long> {

  /**
   * Method findByUsernameIgnoreCase
   * @param username string username of the user we want to find
   * @return user entity which has the specific username
   */
  Optional<UserEntity> findByUsernameIgnoreCase(String username);

  /**
   * Method findByEmailIgnoreCase
   * @param email string email of the user we want to find
   * @return user entity which has the specific email
   */
  Optional<UserEntity> findByEmailIgnoreCase(String email);

  /**
   * Method updateUserNameEmail
   * @param name string, the new name string the user wants
   * @param email string, the new email string the user wants
   * @param userId long id, which identifies which user wants to update info
   */
  @Modifying
  @Query("update UserEntity u set u.username= ?1, u.email= ?2 where u.id= ?3")
  void updateUserNameEmail(String name,  String email, Long userId);

  /**
   * Method updateUserPassword
   * @param password string, the new password the user wants
   * @param userId long id, which identifies which user wants to update password
   */
  @Modifying
  @Query("update UserEntity u set u.password= ?1 where u.id= ?2")
  void updateUserPassword( String password, Long userId);


}