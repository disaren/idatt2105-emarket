package edu.ntnu.idatt2105.backend.config;

import com.auth0.jwt.exceptions.JWTDecodeException;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import edu.ntnu.idatt2105.backend.repository.UserRepository;
import edu.ntnu.idatt2105.backend.service.JWTService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

/**
 * JWT request filter class,
 * creates a tolkien for logged-in user
 */
@Component
public class JWTRequestFilter extends OncePerRequestFilter {

  private JWTService jwtService;
  private UserRepository userRepository;

  /**
   * Constructor JWTRequestFilter
   * @param jwtService sets new jwt service from jwtService param
   * @param userRepository sets new user repository from userRepository param
   */
  public JWTRequestFilter(JWTService jwtService, UserRepository userRepository) {
    this.jwtService = jwtService;
    this.userRepository = userRepository;
  }


  /**
   * Method doFilterInternal
   * @param request a http request
   * @param response the http requests response
   * @param filterChain filterChain used for filtering jwt
   * @throws ServletException gets thrown if the server request or response fails
   * @throws IOException gets thrown if the user is not found
   */
  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

    String tokenHeader = request.getHeader("Authorization");
    if(tokenHeader != null && tokenHeader.startsWith("Bearer ")){
      String token = tokenHeader.substring(7);
      try{
        String username = jwtService.getUsername(token);
        Optional<UserEntity> optionalUserEntity = userRepository.findByUsernameIgnoreCase(username);

        if(optionalUserEntity.isPresent()){
          UserEntity user = optionalUserEntity.get();
          UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(user, null, new ArrayList());
          authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
          SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        }

      }catch (JWTDecodeException ignored){
      }

    }

    filterChain.doFilter(request, response);
  }
}
