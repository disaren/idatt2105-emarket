package edu.ntnu.idatt2105.backend.controller;


import edu.ntnu.idatt2105.backend.model.ChatEntity;
import edu.ntnu.idatt2105.backend.model.ChatMessageEntity;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import edu.ntnu.idatt2105.backend.model.api.MessageBody;
import edu.ntnu.idatt2105.backend.service.ChatService;
import edu.ntnu.idatt2105.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * ChatController class,
 * contains endpoints handling chats between users
 */
@RestController
@EnableAutoConfiguration
@CrossOrigin("*")
@RequestMapping(value = "/api/chat")
public class ChatController {

  @Autowired
  private ChatService chatService;

  @Autowired
  private UserService userService;


  /**
   * Method GetAllChats
   * @param user authentication tolkin for specific user entity
   * @return list with all chat entities for specific user
   */
  @GetMapping("/")
  public List<ChatEntity> getAllChats(@AuthenticationPrincipal UserEntity user){
    return chatService.getAllChatsByUser(user);
  }

  /**
   * Method createChat
   * @param user authentication tolkin for specific user entity
   * @param userId parameter containing long user id
   * @return response entity, depending on chat service status found chat or not
   */
  @PostMapping("/create")
  public ResponseEntity<?> createChat(@AuthenticationPrincipal UserEntity user, @RequestParam("userId") Long userId){
    UserEntity contactUser = userService.getUser(userId);

    if(contactUser == null){
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Couldn't retrieve target user, unable to create chat");
    } else if (Objects.equals(contactUser.getId(), user.getId())) {
      return ResponseEntity.status(HttpStatus.CONFLICT).body("Cant create chat with yourself");
    } else if (chatService.chatAlreadyExists(user,contactUser) || chatService.chatAlreadyExists(contactUser,user)) {
      return ResponseEntity.ok("Chat already exists");
    }

    ChatEntity chat = new ChatEntity();
    chat.setUserOne(user);
    chat.setUserTwo(contactUser);

    chatService.saveChat(chat);
    return ResponseEntity.ok("Chat created");

  }

  /**
   * Method getMessage
   * @param id long id to get specific message
   * @return response entity, depending on chat service status found message or not
   */
  @GetMapping("/messages/{id}")
  public ResponseEntity<?> getMessages(@PathVariable Long id){
    ChatEntity chat = chatService.getChatById(id);
    if(chat == null){
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Chat not found");
    }

    List<ChatMessageEntity> chatMessages = chatService.getAllMessagesFromChat(chat);
    List<MessageBody> messageBodies = new ArrayList<>();

    chatMessages.forEach(chatString -> {
      messageBodies.add(new MessageBody(chatString.getUser().getUsername(),chatString.getMessage()));
    });

    return ResponseEntity.ok(messageBodies);

  }

  /**
   * Method sendMessage
   * @param id long id to get specific message
   * @param user authentication tolkin for specific user entity
   * @param message parameter string message from user
   * @return response entity, depending on chat service status found chat or not
   */
  @PostMapping("/messages/send/{id}")
  public ResponseEntity<?> sendMessage(@PathVariable Long id, @AuthenticationPrincipal UserEntity user, @RequestParam("message") String message){
    ChatEntity chat = chatService.getChatById(id);
    if(chat == null){
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Chat not found");
    }

    ChatMessageEntity chatMessageEntity = new ChatMessageEntity();
    chatMessageEntity.setChat(chat);
    chatMessageEntity.setUser(user);
    chatMessageEntity.setMessage(message);

    chatService.saveMessage(chatMessageEntity);
    return ResponseEntity.ok(chatMessageEntity);
  }

}
