package edu.ntnu.idatt2105.backend.model.enums;

public enum Category {

  FURNITURE,
  CLOTHES,
  ELECTRONICS,
  SPORT

}
