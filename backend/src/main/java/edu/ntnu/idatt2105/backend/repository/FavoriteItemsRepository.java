package edu.ntnu.idatt2105.backend.repository;

import edu.ntnu.idatt2105.backend.model.FavoriteItemEntity;
import edu.ntnu.idatt2105.backend.model.ItemEntity;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

/**
 * FavoriteItemsRepository connecting favoriteItems entity to database
 */
public interface FavoriteItemsRepository extends JpaRepository<FavoriteItemEntity, Long> {

    /**
     * Method findFavoritesByUser
     * @param user entity of the user which wants to find favorites
     * @return list of favorite items of specific user
     */
    List<FavoriteItemEntity> findFavoritesByUser(UserEntity user);

}
