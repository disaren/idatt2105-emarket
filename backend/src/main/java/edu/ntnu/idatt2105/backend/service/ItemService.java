package edu.ntnu.idatt2105.backend.service;

import edu.ntnu.idatt2105.backend.model.ItemEntity;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import java.util.List;

/**
 * ItemService interface class,
 * containing functionality of ItemServiceImplementation
 */
public interface ItemService {
  /**
   * Method getAllItems
   * @return list of items
   */
  public List<ItemEntity> getAllItems();

  /**
   * Method getItems
   * @param id long of item
   * @return item entity which has specific id
   */
  public ItemEntity getItem(Long id);

  /**
   * Method getItemByUser
   * @param user entity of specific user which we want to get items of
   * @return list of items of specific user
   */
  public List<ItemEntity> getItemByUser(UserEntity user) ;

  /**
   * Method getItemByCategory
   * @param category string of specific category which we want to get items of
   * @return list of items with the specific category
   */
  List<ItemEntity> getItemByCategory(String category);

  /**
   * Method saveOrUpdate
   * @param itemEntity entity of the item which we want to save or update
   */
  public void saveOrUpdate(ItemEntity itemEntity);


}
