package edu.ntnu.idatt2105.backend.repository;

import edu.ntnu.idatt2105.backend.model.ChatEntity;
import edu.ntnu.idatt2105.backend.model.ChatMessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

/**
 * MessageRepository class connection message entity to database
 */
public interface MessageRepository extends JpaRepository<ChatMessageEntity, Long> {

  /**
   * Method findByChat
   * @param chatEntity specific of the messages we want
   * @return list of messages in specific chat
   */
  List<ChatMessageEntity> findByChat(ChatEntity chatEntity);

}
