package edu.ntnu.idatt2105.backend.controller;

import edu.ntnu.idatt2105.backend.model.FavoriteItemEntity;
import edu.ntnu.idatt2105.backend.model.ItemEntity;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import edu.ntnu.idatt2105.backend.service.FavoriteItemService;
import edu.ntnu.idatt2105.backend.service.ItemService;
import edu.ntnu.idatt2105.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Objects;


/**
 * FavoriteItem Controller class,
 * containing endpoints handling a user favorite items
 */
@RestController
@EnableAutoConfiguration
@CrossOrigin("*")
@RequestMapping(value = "/api/favorites")
    public class FavoriteItemsController {

    @Autowired
    private FavoriteItemService favoriteItemService;
    @Autowired
    private ItemService itemService;
    @Autowired
   private UserService userService;


    /**
     * Method addFavorite
     * @param user authentication tolkin for specific user entity
     * @param itemID parameter containing long item id, identifies item
     * @return response of http request
     */
    @CrossOrigin("*")
        @PostMapping("/add")
        public ResponseEntity<?> addFavorite(@AuthenticationPrincipal UserEntity user, @RequestParam("itemID")long itemID) {
            favoriteItemService.addToFavorite( new FavoriteItemEntity(userService.getUser(user.getId()), itemService.getItem(itemID)));
            return ResponseEntity.ok().build();
        }

    /**
     * Method removeFavorite
     * @param user authentication tolkin for specific user entity
     * @param itemID parameter containing long item id, identifies item
     * @return response of http request
     */
    @CrossOrigin("*")
    @PostMapping("/remove")
    public ResponseEntity<?> removeFavorite(@AuthenticationPrincipal UserEntity user, @RequestParam("itemID")long itemID) {
        favoriteItemService.removeFromFavorite(user.getId(), itemID);
        return ResponseEntity.ok().build();
    }

    /**
     * Method getFavoritesByUser
     * @param user authentication tolkin for specific user entity
     * @return a list which cointains all the favorite items of a specific user
     */
    @CrossOrigin("*")
    @GetMapping("/me")
    public List<ItemEntity> getFavoritesByUser(@AuthenticationPrincipal UserEntity user){
        return favoriteItemService.getFavoritesByUser(user);
    }
}
