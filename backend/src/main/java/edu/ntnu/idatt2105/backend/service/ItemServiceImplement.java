package edu.ntnu.idatt2105.backend.service;

import edu.ntnu.idatt2105.backend.model.ItemEntity;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import edu.ntnu.idatt2105.backend.repository.ItemRepository;
import edu.ntnu.idatt2105.backend.repository.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * ItemServiceImplements,
 * containing methods for item functionality
 */
@Service
@Transactional
public class ItemServiceImplement implements ItemService{

  private ItemRepository itemRepository;
  private UserRepository userRepository;

  /**
   * ItemServiceImplements constructor
   * @param itemRepository representing item table in database
   * @param userRepository representing user table in database
   */
  public ItemServiceImplement(ItemRepository itemRepository, UserRepository userRepository){
    this.itemRepository = itemRepository;
    this.userRepository = userRepository;
  }

  /**
   * Method getAllItems
   * @return list of all items in database
   */
  @Override
  public List<ItemEntity> getAllItems() {
    return (List<ItemEntity>) itemRepository.findAll();
  }

  /**
   * Method getItems
   * @param id long of item
   * @return item entity which has specific id
   */
  @Override
  public ItemEntity getItem(Long id) {
    return itemRepository.findById(id).orElse(null);
  }

  /**
   * Method getItemByUser
   * @param user entity of specific user which we want to get items of
   * @return list of items of specific user
   */
  @Override
  public List<ItemEntity> getItemByUser(UserEntity user) {
    return itemRepository.findAllByUser(user);
  }

  /**
   * Method getItemByCategory
   * @param category string of specific category which we want to get items of
   * @return list of items with the specific category
   */
  @Override
  public List<ItemEntity> getItemByCategory(String category) {
    return itemRepository.findByCategory(category);
  }

  /**
   * Method saveOrUpdate
   * @param itemEntity entity of the item which we want to save or update
   */
  @Override
  public void saveOrUpdate(ItemEntity itemEntity) {
    itemRepository.save(itemEntity);
  }


}
