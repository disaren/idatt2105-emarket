package edu.ntnu.idatt2105.backend.repository;

import edu.ntnu.idatt2105.backend.model.ItemEntity;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * ItemRepository class connecting Item entity to database
 */
public interface ItemRepository extends JpaRepository<ItemEntity, Long> {

  /**
   * Method findByCategory
   * @param category string category of the items which we want
   * @return a list of items of the given category
   */
  List<ItemEntity> findByCategory(String category);

  /**
   * Method findAllByUser
   * @param user entity of user which we want to find all items
   * @return list of all items which specific user sells
   */
  List<ItemEntity> findAllByUser(UserEntity user);

}
