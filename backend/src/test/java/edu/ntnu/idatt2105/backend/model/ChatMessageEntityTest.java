package edu.ntnu.idatt2105.backend.model;

import edu.ntnu.idatt2105.backend.BackendApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = BackendApplication.class)
class ChatMessageEntityTest {

  ChatMessageEntity message;

  @BeforeEach
  void setUp() {

    ChatEntity chat = new ChatEntity();
    UserEntity userOne = new UserEntity();
    UserEntity userTwo = new UserEntity();

    userOne.setUsername("Test1");
    userTwo.setUsername("Test2");

    chat.setUserOne(userOne);
    chat.setUserTwo(userTwo);

    message = new ChatMessageEntity();
    message.setMessage("yo");
    message.setChat(chat);
    message.setUser(userOne);

  }

  @Test
  void getChat() {
    assertNotNull(message.getChat());
  }

  @Test
  void setChat() {
    ChatEntity previousChat = message.getChat();
    ChatEntity newChat = new ChatEntity();
    message.setChat(newChat);

    assertNotEquals(newChat,previousChat);
  }

  @Test
  void getUser() {
    assertEquals("Test1", message.getUser().getUsername());
  }

  @Test
  void setUser() {
    UserEntity newUser = new UserEntity();
    newUser.setUsername("newUser");
    message.setUser(newUser);
    assertEquals("newUser", message.getUser().getUsername());
  }

  @Test
  void getMessage() {
    assertEquals("yo",message.getMessage());
  }

  @Test
  void setMessage() {
    message.setMessage("new message");
    assertEquals("new message",message.getMessage());
  }
}