package edu.ntnu.idatt2105.backend.model;

import edu.ntnu.idatt2105.backend.BackendApplication;
import edu.ntnu.idatt2105.backend.model.enums.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = BackendApplication.class)
class ItemEntityTest {

  ItemEntity item;
  File file = new File(System.getProperty("user.dir") + System.getProperty("file.separator") +
          "src" + System.getProperty("file.separator") + "main" + System.getProperty("file.separator") +
          "resources" + System.getProperty("file.separator") + "images"  + System.getProperty("file.separator") + "sofa.jpg");

  @BeforeEach
  void setUp() throws IOException {
    UserEntity userOne = new UserEntity();
    userOne.setUsername("Test1");

    item = new ItemEntity();
    item.setCategory(Category.FURNITURE);
    item.setUser(userOne);
    item.setLatitude(2997);
    item.setLongtitude(58492);
    item.setPrice(499);
    item.setName("Ice fan");
    item.setShortDesc("An electrical fan made out of ice");
    item.setLongDesc("An electrical fan made out of ice is a unique and innovative concept that combines the traditional cooling properties of ice with the modern convenience of an electric fan. This type of fan is designed to keep you cool during hot and humid weather, while also providing a visually stunning and refreshing experience.\n" +
            "\n" +
            "The fan is made entirely of ice, which is carefully shaped and sculpted to create the perfect form for efficient airflow. The ice is made using purified water and frozen in a special mold that is designed to create a smooth and uniform surface. The mold is also designed to ensure that the ice is strong enough to withstand the pressure and rotation of the fan blades.\n" +
            "\n" +
            "The fan blades themselves are made of a sturdy, yet lightweight material that is carefully attached to the ice frame. The blades are designed to provide a steady and consistent flow of air, while also minimizing any vibrations or noise.");
    item.setImage(Base64.getEncoder().encodeToString(Files.readAllBytes(file.toPath())));
    item.setId(1);
  }

  @Test
  void getId() {
    assertEquals(1,item.getId());
  }

  @Test
  void setId() {
    item.setId(2);
    assertEquals(2,item.getId());
  }

  @Test
  void getName() {
    assertEquals("Ice fan", item.getName());
  }

  @Test
  void setName() {
    item.setName("New name");
    assertEquals("New name", item.getName());
  }

  @Test
  void getShortDesc() {
    assertEquals("An electrical fan made out of ice", item.getShortDesc());
  }

  @Test
  void setShortDesc() {
    item.setShortDesc("new short description");
    assertEquals("new short description",item.getShortDesc());
  }

  @Test
  void getLongDesc() {
    assertEquals("An electrical fan made out of ice is a unique and innovative concept that combines the traditional cooling properties of ice with the modern convenience of an electric fan. This type of fan is designed to keep you cool during hot and humid weather, while also providing a visually stunning and refreshing experience.\n" +
            "\n" +
            "The fan is made entirely of ice, which is carefully shaped and sculpted to create the perfect form for efficient airflow. The ice is made using purified water and frozen in a special mold that is designed to create a smooth and uniform surface. The mold is also designed to ensure that the ice is strong enough to withstand the pressure and rotation of the fan blades.\n" +
            "\n" +
            "The fan blades themselves are made of a sturdy, yet lightweight material that is carefully attached to the ice frame. The blades are designed to provide a steady and consistent flow of air, while also minimizing any vibrations or noise.", item.getLongDesc());
  }

  @Test
  void setLongDesc() {
    item.setLongDesc("new long description");
    assertEquals("new long description", item.getLongDesc());
  }

  @Test
  void getCategory() {
    assertEquals(Category.FURNITURE, item.getCategory());
  }

  @Test
  void setCategory() {
    item.setCategory(Category.CLOTHES);
    assertEquals(Category.CLOTHES, item.getCategory());
  }

  @Test
  void getLatitude() {
    assertEquals(2997, item.getLatitude());
  }

  @Test
  void setLatitude() {
    item.setLatitude(42);
    assertEquals(42,item.getLatitude());
  }

  @Test
  void getLongtitude() {
    assertEquals(58492, item.getLongtitude());
  }

  @Test
  void setLongtitude() {
    item.setLongtitude(42);
    assertEquals(42, item.getLongtitude());
  }

  @Test
  void getPrice() {
    assertEquals(499, item.getPrice());
  }

  @Test
  void setPrice() {
    item.setPrice(399);
    assertEquals(399,item.getPrice());
  }

  @Test
  void getUser() {
    assertEquals("Test1",item.getUser().getUsername());
  }

  @Test
  void setUser() {
    UserEntity newUser = new UserEntity();
    item.setUser(newUser);
    assertEquals(newUser, item.getUser());
  }

  @Test
  void getImage() throws IOException {
    String imageString = Base64.getEncoder().encodeToString(Files.readAllBytes(file.toPath()));
    assertEquals(imageString, item.getImage());
  }
}