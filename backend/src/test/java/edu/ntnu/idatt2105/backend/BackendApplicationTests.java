package edu.ntnu.idatt2105.backend;

import edu.ntnu.idatt2105.backend.model.ChatEntity;
import edu.ntnu.idatt2105.backend.model.ChatMessageEntity;
import edu.ntnu.idatt2105.backend.model.ItemEntity;
import edu.ntnu.idatt2105.backend.model.UserEntity;
import edu.ntnu.idatt2105.backend.model.enums.Category;
import edu.ntnu.idatt2105.backend.repository.ChatRepository;
import edu.ntnu.idatt2105.backend.repository.ItemRepository;
import edu.ntnu.idatt2105.backend.repository.MessageRepository;
import edu.ntnu.idatt2105.backend.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Base64;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BackendApplicationTests {

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ChatRepository chatRepository;

    @Autowired
    MessageRepository messageRepository;

    File file = new File(System.getProperty("user.dir") + System.getProperty("file.separator") +
            "src" + System.getProperty("file.separator") + "main" + System.getProperty("file.separator") +
            "resources" + System.getProperty("file.separator") + "images"  + System.getProperty("file.separator") + "sofa.jpg");


    UserEntity user;
    ChatEntity chat;

    @Test
    void saveUser(){
        user = new UserEntity();
        user.setUsername("Test1");
        user.setPassword("password123");
        user.setEmail("test@gmail.com");
        user.setModerator(true);

        userRepository.save(user);
        assertNotNull(userRepository.findByUsernameIgnoreCase("Test1").get());
        assertEquals("Test1", userRepository.findByUsernameIgnoreCase("Test1").get().getUsername());
        assertEquals("password123", userRepository.findByUsernameIgnoreCase("Test1").get().getPassword());
        assertEquals("test@gmail.com", userRepository.findByUsernameIgnoreCase("Test1").get().getEmail());
        assertTrue(userRepository.findByUsernameIgnoreCase("Test1").get().isModerator());

    }

    @Test
    void saveChat(){
        chat = new ChatEntity();
        chat.setUserOne(user);
        chat.setUserTwo(user);

        chatRepository.save(chat);

        assertNotNull(chatRepository.findById(1L).get());
        assertEquals(user, chatRepository.findById(1L).get().getUserOne());
        assertEquals(user, chatRepository.findById(1L).get().getUserTwo());
    }

    @Test
    void saveMessage(){
        ChatMessageEntity chatMessageEntity = new ChatMessageEntity();
        chatMessageEntity.setMessage("yo");
        chatMessageEntity.setChat(chat);
        chatMessageEntity.setUser(user);

        messageRepository.save(chatMessageEntity);
        assertNotNull(messageRepository.findById(1L).get());
        assertEquals("yo", messageRepository.findById(1L).get().getMessage());
        assertEquals(chat, messageRepository.findById(1L).get().getChat());
        assertEquals(user, messageRepository.findById(1L).get().getUser());
    }
}
