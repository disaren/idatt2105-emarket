package edu.ntnu.idatt2105.backend;

import edu.ntnu.idatt2105.backend.repository.UserRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserControllerTest {


    @LocalServerPort
    private int port;

    private String baseUrl = "http://localhost";

    private static RestTemplate restTemplate;

    @Autowired
    private UserRepository userRepository;

    @BeforeAll
    public static void init(){
        restTemplate = new RestTemplate();
    }

    @BeforeEach
    public void beforeSetup() {
        baseUrl = baseUrl + ":" + port + "/api/users";
    }

    @AfterEach
    public void afterSetup() {
        userRepository.deleteAll();
    }

    @Test
    void save() throws JSONException {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        JSONObject userJsonObject = new JSONObject();
        userJsonObject.put("username","Olaf");
        userJsonObject.put("password", "hei123");
        userJsonObject.put("email", "olaf@gmail.com");

        HttpEntity<String> request = new HttpEntity<>(userJsonObject.toString(),headers);
        String result = restTemplate.postForObject(baseUrl + "/add",request,String.class);

        assertEquals("User added", result);

    }

    @Test
    void loginUserAfterAddingUser() throws JSONException {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        JSONObject userJsonObject = new JSONObject();
        userJsonObject.put("username","Olaf");
        userJsonObject.put("password", "hei123");
        userJsonObject.put("email", "olaf@gmail.com");

        HttpEntity<String> request = new HttpEntity<>(userJsonObject.toString(),headers);
        String resultOne = restTemplate.postForObject(baseUrl + "/add",request,String.class);

        headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        JSONObject loginJsonObject = new JSONObject();
        loginJsonObject.put("username","Olaf");
        loginJsonObject.put("password", "hei123");

        request = new HttpEntity<>(loginJsonObject.toString(),headers);
        System.out.println(request);

        String resultTwo = restTemplate.postForObject(baseUrl + "/auth/login",request,String.class);
        assert resultTwo != null;

        assertTrue(resultTwo.contains("jwt"));

    }

    @Test
    void list() throws JSONException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        JSONObject userJsonObject = new JSONObject();
        userJsonObject.put("username","Olaf");
        userJsonObject.put("password", "hei123");
        userJsonObject.put("email", "olaf@gmail.com");

        HttpEntity<String> request = new HttpEntity<>(userJsonObject.toString(),headers);
        String resultOne = restTemplate.postForObject(baseUrl + "/add",request,String.class);



        headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        JSONObject loginJsonObject = new JSONObject();
        loginJsonObject.put("username","Olaf");
        loginJsonObject.put("password", "hei123");

        request = new HttpEntity<>(loginJsonObject.toString(),headers);
        System.out.println(request);

        String resultTwo = restTemplate.postForObject(baseUrl + "/auth/login",request,String.class);
        assert resultTwo != null;
        String jwtToken = resultTwo.substring(resultTwo.indexOf("jwt") + 6, resultTwo.indexOf("moderator") - 3);


        headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + jwtToken);
        headers.add("Content-Type", "application/json");

        request = new HttpEntity<>(headers);



        ResponseEntity<String> resultThree = restTemplate.exchange(baseUrl + "/list",HttpMethod.GET,request,String.class);

        assertTrue(Objects.requireNonNull(resultThree.getBody()).length() > 0);



    }








}