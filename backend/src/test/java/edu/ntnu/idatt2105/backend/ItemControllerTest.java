package edu.ntnu.idatt2105.backend;

import edu.ntnu.idatt2105.backend.repository.ItemRepository;
import org.json.JSONException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ItemControllerTest {
    @LocalServerPort
    private int port;

    private String baseUrl = "http://localhost";

    private static RestTemplate restTemplate;

    @Autowired
    private ItemRepository itemRepository;

    @BeforeAll
    public static void init(){
        restTemplate = new RestTemplate();
    }

    @BeforeEach
    public void beforeSetup() {
        baseUrl = baseUrl + ":" + port + "/api/items";
    }

    @AfterEach
    public void afterSetup() {
        itemRepository.deleteAll();
    }
    @Test
    void list() throws JSONException {


        List resultOne = restTemplate.getForObject(baseUrl + "/list",List.class);
        assert resultOne != null;
        assertTrue(resultOne.size() == 0 || resultOne.size() == 1);

    }

    @Test
    void getCategories() {

        List resultOne = restTemplate.getForObject(baseUrl + "/categories",List.class);
        assert resultOne != null;
        assertTrue(resultOne.size() > 0);

    }
}