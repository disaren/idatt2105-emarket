package edu.ntnu.idatt2105.backend.model;

import edu.ntnu.idatt2105.backend.BackendApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = BackendApplication.class)
class ChatEntityTest {

  ChatEntity chat;
  @BeforeEach
  void setUp() {
    UserEntity userOne = new UserEntity();
    UserEntity userTwo = new UserEntity();

    userOne.setUsername("Test1");
    userTwo.setUsername("Test2");

    chat = new ChatEntity();
    chat.setUserOne(userOne);
    chat.setUserTwo(userTwo);

  }

  @Test
  void getUserOne() {
    assertEquals("Test1", chat.getUserOne().getUsername());
  }

  @Test
  void setUserOne() {
    UserEntity newUserOne = new UserEntity();
    newUserOne.setUsername("NewUser");
    chat.setUserOne(newUserOne);

    assertEquals("NewUser", chat.getUserOne().getUsername());

  }

  @Test
  void getUserTwo() {
    assertEquals("Test2", chat.getUserTwo().getUsername());
  }

  @Test
  void setUserTwo() {
    UserEntity newUserTwo = new UserEntity();
    newUserTwo.setUsername("NewUser");
    chat.setUserTwo(newUserTwo);

    assertEquals("NewUser", chat.getUserTwo().getUsername());

  }
}