package edu.ntnu.idatt2105.backend.model;

import edu.ntnu.idatt2105.backend.BackendApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = BackendApplication.class)
class UserEntityTest {

  UserEntity user;
  @BeforeEach
  void setUp() {
    user = new UserEntity();
    user.setUsername("Test1");
    user.setPassword("password123");
    user.setEmail("test1@gmail.com");
    user.setModerator(false);
  }

  @Test
  void getUsername() {
    assertEquals("Test1", user.getUsername());
  }

  @Test
  void setUsername() {
    user.setUsername("new name");
    assertEquals("new name", user.getUsername());
  }

  @Test
  void getPassword() {
    assertEquals("password123", user.getPassword());
  }

  @Test
  void setPassword() {
    user.setPassword("newPassword");
    assertEquals("newPassword",user.getPassword());
  }

  @Test
  void getEmail() {
    assertEquals("test1@gmail.com",user.getEmail());
  }

  @Test
  void setEmail() {
    user.setEmail("new@gmail.com");
    assertEquals("new@gmail.com",user.getEmail());
  }

  @Test
  void isModerator() {
    assertFalse(user.isModerator());
  }

  @Test
  void setModerator() {
    user.setModerator(true);
    assertTrue(user.isModerator());
  }
}