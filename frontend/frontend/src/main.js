import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import {useUserStore} from "@/stores/user";

const pinia = createPinia();
const app = createApp(App)

app.use(pinia)
app.use(router)

export const userStore = useUserStore();

app.mount('#app')

