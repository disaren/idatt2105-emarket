import { ref, watch } from "vue";
import { defineStore } from "pinia";

export const useUserStore = defineStore("user", () => {
  const user = ref({
    name: "",
    email: "",
    password:"",
    admin:false,
    loggedIn:false,
    jwt: "",
    id:0,
    favorites: [],
  });

  if (localStorage.getItem("user")) {
    user.value = JSON.parse(localStorage.getItem("user"));
  }

  watch(
      user,
      (userVal) => {
        localStorage.setItem("user", JSON.stringify(userVal));
      },
      {deep:true}
  );

  const changeName = (newName) => {
    user.value.name = newName;
  };

  const changeEmail = (changeEmail) => {
    user.value.email = changeEmail;
  };

  const changePassword = (changePassword) => {
    user.value.password = changePassword;
  };

  const changeAdminStatus = (changeAdminStatus) => {
    user.value.admin = changeAdminStatus;
  };

  const changeLoggInStatus = (changeLoggInStatus) => {
    user.value.loggedIn = changeLoggInStatus;
  };

  const setId = (setId) => {
    user.value.id = setId;
  };

  const setJwt = (setJwt) => {
    user.value.jwt = setJwt;
  };

  return {
    user,
    changeName,
    changeEmail,
    changePassword,
    changeAdminStatus,
    changeLoggInStatus,
    setId,
    setJwt
  };
});