import axios from "axios";

export const createUser = async (user) => {
    return (await axios.post(`http://localhost:8081/api/users/add`, user)).data
}

export const loginUser = async (user) => {
    return (await axios.post(`http://localhost:8081/api/users/auth/login`, user)).data
}