import logInView from "@/views/userView/LogInView.vue";
import {mount} from "@vue/test-utils";
import {beforeEach, describe, expect, test} from 'vitest'
import {userStore} from '@/main' ;
import axios from "axios";
import { vi } from 'vitest'
import {loginUser} from "@/components/__tests__/user.service";
vi.mock('axios')



describe('Data Store Test', () => {
    let wrapper = mount(logInView);

    beforeEach(() => {
        axios.get.mockReset()
        axios.post.mockReset()
    })

    describe('loginUser', () => {
        test('makes a POST request to create a new user', async () => {
            const loginUserPayload = {
                username: 'Kari',
                password: 'kari123',
            }

            const loginUserMock = {
                "username": "Kari",
                "password": "kari123",
            }

            axios.post.mockResolvedValue({
                data: loginUserMock,
            })

            const newLoginUser = await loginUser(loginUserPayload)

            expect(newLoginUser).equals(loginUserMock)
        })

    })

    test("login unsuccessful", async () => {
        await wrapper.find('input[id="username"]').setValue("");
        await wrapper.find('input[id="password"]').setValue("kari123");
        await wrapper.find('button').trigger('click');

        expect(wrapper.find('#alert').text() === "incorrect password or username")
    })

    test('user logged in false', () => {
        expect(userStore.user.loggedIn).toBe(false);
    })

    test("Contains all input fields", () => {
        expect(wrapper.find('input[id="username"]').html());
        expect(wrapper.find('input[id="password"]').html());
    })

    test("alert string empty", () => {
        expect(wrapper.find('#alert').text() === "");
    })

})


