import register from "@/views/userView/RegisterUserView.vue";
import {mount} from "@vue/test-utils";
import { vi } from 'vitest'
import { describe, expect, test, beforeEach } from 'vitest'
import {userStore} from '@/main' ;
import axios from "axios";
import { createUser } from './user.service'
vi.mock('axios')


describe('Data Store Test', () => {
    let wrapper = mount(register);
    beforeEach(() => {
        axios.get.mockReset()
        axios.post.mockReset()
    })

    describe('createUser', () => {
        test('makes a POST request to create a new user', async () => {
            const newUserPayload = {
                username: 'Karl',
                password: 'karl123',
                email:'karl@gmail.com'
            }

            const newUserMock = {
                "username": "Karl",
                "password": "karl123",
                "email": "karl@gmail.com"
            }

            axios.post.mockResolvedValue({
                data: newUserMock,
            })

            const newUser = await createUser(newUserPayload)

            expect(newUser).equals(newUserMock)
        })

    })

    test("Contains all input fields", () => {
        expect(wrapper.find('input[id="email"]').html());
        expect(wrapper.find('input[id="name"]').html());
        expect(wrapper.find('input[id="password"]').html());
    })

    test("Successful register sends to login page", async () => {
        await wrapper.find('input[id="email"]').setValue("Knut@gmail.com");
        await wrapper.find('input[id="name"]').setValue("Knut");
        await wrapper.find('input[id="password"]').setValue("knut123");
        await wrapper.find('button').trigger('click');

        expect(wrapper.find('h2').text() === "Login")

    })

    test("Cannot register user without all information", async () => {
        await wrapper.find('input[id="email"]').setValue("Knut@gmail.com");
        await wrapper.find('input[id="name"]').setValue("knut");
        await wrapper.find('input[id="password"]').setValue("k");

        expect(wrapper.find('button') === null)

    })

    test("alert string empty", () => {
        expect(wrapper.find('#alert').text() === "");
    })

})