import UploadItemTest from "../../views/itemView/UploadItemTest.vue";
import {mount} from "@vue/test-utils";
import {describe,test,expect,beforeEach} from "vitest";
import {userStore} from '@/main' ;

describe("Test homepage components", () => {
    let wrapper = null;

    beforeEach(() => {
        const store = userStore;
        store.user.name = "Test";

        wrapper = mount(UploadItemTest);
    })

    test("Contains title", () => {
        expect(wrapper.find("h1").html()).contains("Upload item for sale!");
    })

    test("Contains all input fields", () => {
        expect(wrapper.find('input[id="name"]').html());
        expect(wrapper.find('input[id="shortDesc"]').html());
        expect(wrapper.find('input[id="price"]').html());
        expect(wrapper.find('textarea[id="longDesc"]').html());
        expect(wrapper.find('input[id="latitude"]').html());
        expect(wrapper.find('input[id="longtitude"]').html());
        expect(wrapper.find('select[id="category"]').html());
        expect(wrapper.find('input[type="file"]').html());
    })

    test("Input wrong value for price", async () => {
        await wrapper.find('input[id="name"]').setValue("vitest name");
        await wrapper.find('input[id="shortDesc"]').setValue("vitest short description");
        await wrapper.find('input[id="price"]').setValue(-5);
        await wrapper.find('textarea[id="longDesc"]').setValue("vitest long description");
        await wrapper.find('input[id="latitude"]').setValue(10);
        await wrapper.find('input[id="longtitude"]').setValue(40);
        await wrapper.find('select[id="category"]').setValue("FURNITURE");
        await wrapper.find('input[type="file"]').setValue("");

        await wrapper.find('button').trigger('click');

        await expect(wrapper.find('.alert').text()).contains("Values cannot be negative!")

    })

    test("Empty input fields", async () => {
        await wrapper.find('input[id="name"]').setValue("");
        await wrapper.find('input[id="shortDesc"]').setValue("vitest short description");
        await wrapper.find('input[id="price"]').setValue(5);
        await wrapper.find('textarea[id="longDesc"]').setValue("");
        await wrapper.find('input[id="latitude"]').setValue(10);
        await wrapper.find('input[id="longtitude"]').setValue(40);
        await wrapper.find('select[id="category"]').setValue("FURNITURE");
        await wrapper.find('input[type="file"]').setValue("");

        await wrapper.find('button').trigger('click');

        await expect(wrapper.find('.alert').text()).contains("No input fields can be blank!")

    })

    test("Successfully upload item", async () => {
        await wrapper.find('input[id="name"]').setValue("");
        await wrapper.find('input[id="shortDesc"]').setValue("vitest short description");
        await wrapper.find('input[id="price"]').setValue(5);
        await wrapper.find('textarea[id="longDesc"]').setValue("");
        await wrapper.find('input[id="latitude"]').setValue(10);
        await wrapper.find('input[id="longtitude"]').setValue(40);
        await wrapper.find('select[id="category"]').setValue("FURNITURE");
        await wrapper.find('input[type="file"]').setValue("");

        await wrapper.find('button').trigger('click');

        await expect(wrapper.find('.alert').text()).contains("")

    })



})