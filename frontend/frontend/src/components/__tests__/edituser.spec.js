import editProfile from "../../views/userView/EditProfile.vue";
import {mount} from "@vue/test-utils";
import {describe,test,expect,beforeEach} from "vitest";
import {userStore} from '@/main' ;

describe("Test Edit user components", () => {
    let wrapper = null;

    beforeEach(() => {
        const store = userStore;
        store.user.name = "Test";

        wrapper = mount(editProfile);
    })

    test("Contains title", () => {
        expect(wrapper.find("h2").html()).contains("Edit profile");
    })

    test("Contains all input fields", () => {
        expect(wrapper.find('input[name="name"]').html());
        expect(wrapper.find('input[name="email"]').html());
        expect(wrapper.find('input[name="password"]').html());
    })

    test("Successfully edit user profile", async () => {
        await wrapper.find('input[name="name"]').setValue("vitest name");
        await wrapper.find('input[name="email"]').setValue("vitest@gmail.com");
        await wrapper.find('input[name="password"]').setValue("hei123");

        await wrapper.find('button').trigger('click');

        await expect(wrapper.find('.alert').text()).contains("")

    })

    test("Empty input fields", async () => {
        expect(wrapper.find("button") == null)
    })

    test("Incorrect email input field format", async () => {
        await wrapper.find('input[name="name"]').setValue("vitest name");
        await wrapper.find('input[name="email"]').setValue("vitest");

        expect(wrapper.find("button") == null)

    })


})