import HomeView from "../../views/HomeView.vue";
import {mount} from "@vue/test-utils";
import {describe,test,expect} from "vitest";



describe("Test homepage components", () => {

    let wrapper = mount(HomeView);

    test("Contains title", () => {
        expect(wrapper.find("h1").html()).contains("eMarket");
    })

    test("Drop down menu contains select", () => {
        expect(wrapper.find('select[id="category"]').html()).contains("SELECT");
    })



})