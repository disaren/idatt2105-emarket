import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/userView/LogInView.vue')
    },
    {
      path: '/uploaditem',
      name: 'uploaditem',
      component: () => import('../views/itemView/UploadItemTest.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: () => import('../views/userView/RegisterUserView.vue')
    },

    {
      path: '/favoritesList',
      name: 'favoritesList',
      component: () => import('../views/itemView/FavoritesListView.vue')
    },

    {
      path: '/chats',
      name: 'chats',
      component: () => import('../views/ChatsVue.vue')
    },
    {
      path: '/modifyCategories',
      name: 'modifyCategories',
      component: () => import('../views/userView/ModifyCategoriesView.vue')
    },
    {
      path: '/listings',
      name: 'listings',
      component: () => import('../views/itemView/MyListings.vue')
    },
    {
      path: '/editProfile',
      name: 'editProfile',
      component: () => import('../views/userView/EditProfile.vue')
    },
    {
      path: '/logout',
      name: 'logOut',
      component: () => import('../views/userView/LogOutView.vue')
    },
    {
      path: '/item/:id',
      name: 'item',
      component: () => import('../views/itemView/ItemDetails.vue')
    }
  ]
})

export default router
