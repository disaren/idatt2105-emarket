

describe('Test login page components', () => {

    beforeEach(() => {
        cy.visit("/login");
    })

    it("Page elements renders in", () => {
        cy.contains("Username");
        cy.contains("Password");
        cy.contains("Login");
    })

    it('Fail to log in', () => {
        cy.get(".input").find(">input").first().type("awdawd");
        cy.get(".input").find(">input").last().type("dsdaete");
        cy.get("#login_button").click();
        cy.contains("incorrect password or username");
    })

    it('Successfully log in', () => {
        cy.get(".input").find(">input").first().type("Kari");
        cy.get(".input").find(">input").last().type("kari123");
        cy.get("#login_button").click();
        cy.contains("incorrect password or username").should("not.exist");
        cy.contains("Edit Profile");
        cy.contains("Chats");
    })

    it("Incorrect login format", () => {
        cy.get(".input").find(">input").first().type("yo");
        cy.get(".input").find(">input").last().type("hei");
        cy.get("#login_button").click();
        cy.contains("incorrect password or username");
    })

    it("Correctly redirects to register page", () => {
        cy.get("span").then(($btn) => {
            cy.wait(2000);
            cy.wrap($btn).click();
        });
        cy.contains("Sign up");
    })
})