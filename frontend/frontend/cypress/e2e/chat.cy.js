

describe('Test chat components', () => {

    beforeEach(() => {
        cy.visit("/register");
        cy.get(".input").find(">input").first().type("test42@gmail.com");
        cy.get(".input").find(">input").eq(1).type("test42");
        cy.get(".input").find(">input").last().type("hei123");
        cy.get("button").click().then(() => {
            cy.wait(2000);
            cy.visit("/login");
            cy.get(".input").find(">input").first().type("test42");
            cy.get(".input").find(">input").last().type("hei123");
            cy.get("#login_button").click();
        });

    })

    it("Successfully send message to item owner", () => {
        cy.visit("/");
        cy.wait(1000);
        cy.get(".item").eq(0).click();
        cy.contains("Contact Seller").click().then(() => {
            cy.get("h1").contains("Here you can see your chats");
        })

        cy.wait(1000);
        cy.get("#chat").click()

        cy.get("form").find(">input").eq(0).type("Hello item owner");
        cy.get("form").find(">input").eq(1).click();

        cy.get("#messageBox").contains("Hello item owner");



    })



})