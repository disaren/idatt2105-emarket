


describe('Test favourite components', () => {

    beforeEach(() => {
        cy.visit("/login");
        cy.get(".input").find(">input").first().type("Kari");
        cy.get(".input").find(">input").last().type("kari123");
        cy.get("#login_button").click();
        cy.wait(1000);
    })

    it("Favourites the default item on the homepage", () => {
        cy.visit("/");
        cy.get("#heart").first().click();
        cy.visit("/favoritesList");
        cy.contains("Grey sofa");
    })



})