


describe('Test homepage components', () => {

    beforeEach(() => {
        cy.visit('/');
    })

    it('Checks to see if page has rendered in', () => {
        cy.contains("eMarket");
        cy.contains("Home");
        cy.contains("Log in");
        cy.contains("Search");
    })

    it('Default item loads in', () => {
        cy.contains("Grey sofa");
    })

    it("Item is intractable and changes to a more detailed page", () => {
        cy.wait(1000);
        cy.get(".flex-container_items").children().first().click();
        cy.contains("Grey sofa with black metal legs from IKEA");
    })

    it("Successfully changes page to login", () => {
        cy.contains("Log in").click();
        cy.contains("Login");
        cy.contains("Username");
    })
})