

describe('Test upload page components', () => {

    beforeEach(() => {
        cy.visit("/login");
        cy.get(".input").find(">input").first().type("Kari");
        cy.get(".input").find(">input").last().type("kari123");
        cy.get("#login_button").click();
        cy.wait(1000);
    })

    it("Successfully uploads an item", () => {
        cy.visit("/uploaditem");
        cy.wait(1000);
        cy.get(".itemForm").find(">input").eq(0).type("Test Name");
        cy.get(".itemForm").find(">input").eq(1).type("Test short description");
        cy.get(".itemForm").find(">input").eq(2).type("1");
        cy.get(".inputForm").find(">input").eq(0).type("2");
        cy.get(".inputForm").find(">input").eq(1).type("3");
        cy.get("textarea").type("Test long description");
        cy.get("select").select(0);
        cy.get(".itemForm").find(">input").eq(3).selectFile({
            contents: Cypress.Buffer.from('file contents'),
            fileName: 'file.txt',
            lastModified: Date.now(),
        });
        cy.get("button").click().then(() => {
            cy.contains("Test Name");
        });
    })

    it("Fails to upload an item due to negative price value", () => {
        cy.visit("/uploaditem");
        cy.get(".itemForm").find(">input").eq(0).type("Test Name");
        cy.get(".itemForm").find(">input").eq(1).type("Test short description");
        cy.get(".itemForm").find(">input").eq(2).type("1");
        cy.get(".inputForm").find(">input").eq(0).type("{backspace}").type("-2");
        cy.get(".inputForm").find(">input").eq(1).type("3");
        cy.get("textarea").type("Test long description");
        cy.get("select").select(0);
        cy.get(".itemForm").find(">input").eq(3).selectFile({
            contents: Cypress.Buffer.from('file contents'),
            fileName: 'file.txt',
            lastModified: Date.now(),
        });
        cy.get("button").click().then(() => {
            cy.contains("Values cannot be negative!");
        });
    })

    it("Fails to upload an item due to empty fields", () => {
        cy.visit("/uploaditem");
        cy.get(".itemForm").find(">input").eq(1).type("Test short description");
        cy.get(".itemForm").find(">input").eq(2).type("1");
        cy.get(".inputForm").find(">input").eq(0).type("{backspace}").type("-2");
        cy.get(".inputForm").find(">input").eq(1).type("3");
        cy.get("select").select(0);
        cy.get(".itemForm").find(">input").eq(3).selectFile({
            contents: Cypress.Buffer.from('file contents'),
            fileName: 'file.txt',
            lastModified: Date.now(),
        });
        cy.get("button").click().then(() => {
            cy.contains("No input fields can be blank!");
        });
    })


})