

describe('Test register page components', () => {

    beforeEach(() => {
        cy.visit("/register");
    })

    it("Page elements render in", () => {
        cy.contains("Sign up");
        cy.contains("Email address");
        cy.contains("Username");
        cy.contains("Password");
    })

    it("Correctly redirects to login page", () => {
        cy.get("span").click();
        cy.get("h2").contains("Login");
    })

    it("Sign up button doesnt show up until form is filled correctly", () => {
        cy.get(".input").find(">input").first().type("daniel@gmail.com");
        cy.get("button").should("not.exist");

        cy.get(".input").find(">input").eq(1).type("Daniel");
        cy.get("button").should("not.exist");

        cy.get(".input").find(">input").last().type("hei123");
        cy.get("button").contains("Sign up");
    })

})