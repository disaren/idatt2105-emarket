describe('Test item details components', () => {

    beforeEach(() => {
        cy.visit('/');
        cy.get(".item").eq(0).click();
    })

    it("Renders in item details", () => {
        cy.contains("Kari");
        cy.contains("1099 kr");
        cy.contains("Grey sofa with black metal legs from IKEA");
    })

    it("Create chatroom function works correctly", () => {
        cy.contains("Contact Seller").click().then(() => {
            cy.get("h1").contains("Here you can see your chats");
        })
    })


})